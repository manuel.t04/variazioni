class Classe{

    articolazione;
    grado;
    sezione;

    constructor(articolazione, sezione, grado) {
        this.articolazione = articolazione;
        this.grado = grado;
        this.sezione = sezione;
    }
}
//SCRIPT CHE PERMETTE DI RICERCARE LE SUPPLENZE DI UNA CLASSE

$("#searchByType").click(getParamsBytype);         //se clicchi il bottone (scritto in JQUERY)

function getParamsBytype() {
    let articolazioni=$("#selectBoxArticolazioni option:selected");
    let sezione=$("#selectBoxSezione option:selected");
    let grado=$("#selectBoxGrado option:selected");
    HTTPreq(new Classe(articolazioni.text(),grado.val(),sezione.text()));
}

//SCRIPT CHE PERMETTE DI RICERCARE LE SUPPLENZE DI UN DOCENTE SPECIFICO
$('#searchByDocente').click(getParamsByDocente);

function getParamsByDocente() {
    let docente = $("#selectBoxDocente option:selected");

    HTTPreq(docente.text());

}


//STRUTTURA DI UNA RICHIESTA HTTP BASE.
function HTTPreq(classe) {

    let json= JSON.stringify(classe); //file Json contenente le informazioni da inviare al server, che dovrà reperire i giusti dati per la corretta classe.
    console.log(json);
    const method='GET';
    const URL=''; //Inserire l'indirizzo del server
    const xml = new XMLHttpRequest();
    xml.open(method, URL, true);
    xml.responseType = "json";      //ritorna una stringa formattata in JSON.

    xml.onload = function () {
        if (xml.status == 200)
            console.log("richiesta effettuata con successo!")
        else {
            console.log("la richiesta non è arrivata. (codice errore: " + xml.status + ").")            //errori 401,404, ecc..
        }
    }

    xml.onerror = function () {
        console.log("la richiesta non è andata a buon fine.")           //quando la richiesta non va a buon fine (non riesce a raggiungere il server).
    }
}



